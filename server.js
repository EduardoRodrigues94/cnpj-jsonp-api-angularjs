var express = require('express');
var app = express();
var port = 8083;
app.use(express.static("CNPJ API"));
app.get('/', function (req, res, next) {
    res.redirect('/');
});
app.listen(port, "192.168.0.16");
console.log("MyProject Server is Listening on port " + port);
