(function () {

    //Controller Function
    var cnpjController = function ($http, $scope, svcJsonp) {

        var vm = this; //Function Bindings

        //$onInit
        vm.$onInit = function () {

        };

        vm.getData = function () {
            //27865757000102 <- cnpj globo
            svcJsonp.jsonp('https://www.receitaws.com.br/v1/cnpj/' + vm.cnpj, function (data) {

                var jsonData = JSON.stringify(data, null, 4);

                if (vm.data != jsonData) {
                    console.log(data);
                    vm.data = jsonData;
                    $scope.$apply();
                }
            });
        }
    };

    //Controller
    angular.module('app').controller('cnpjController', cnpjController);

    //Component
    angular.module('app').component('cnpjController', {
        templateUrl: '../../../../CNPJ API/app/views/cnpj.html',
        controller: 'cnpjController',
        controllerAs: 'vm'
    });
})();
